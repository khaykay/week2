﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace goals_compiled
{     /// <summary>  
        /// Demonstrates  conditional testing, random numbers 
        ///  khay_kay 17-06-26
        ///  </summary>
    class Program
    {
        static void Main(string[] args)
        {
            {
                string name;
                Console.WriteLine("please enter name");
                name = Console.ReadLine();

                if (name == "ayodeji")
                {
                    Console.WriteLine("this is my week two goals");
                }
                else if (name == "kosi")
                {
                    Console.WriteLine("sci is pretty cool");
                }
                else
                {
                    Console.WriteLine("Nice to see you, {0}!", name);
                }
                    Console.WriteLine(" welcome to this simple math game,"+ name); 

                int a, b, c, guess, score;
                score = 0;
                Random myrand = new Random();

                //addition    
                a = (int)(myrand.NextDouble() * 10) + 1;
                b = (int)(myrand.NextDouble() * 10) + 1;
                c = a + b;
                Console.Write("What is {0} + {1}? ", a, b);
                guess = Convert.ToInt32(Console.ReadLine());
                if (guess == c) { score++; }

                //subtraction
                a = (int)(myrand.NextDouble() * 10) + 1;
                b = (int)(myrand.NextDouble() * 10) + 1;
                c = a + b;
                Console.Write("What is {0} − {1}? ", c, a);
                guess = Convert.ToInt32(Console.ReadLine());
                if (guess == b)
                { score++; }

                //multiplication 
                a = (int)(myrand.NextDouble() * 10) + 1;
                b = (int)(myrand.NextDouble() * 10) + 1;
                c = a * b;
                Console.Write("What is {0} * {1}? ", a, b);
                guess = Convert.ToInt32(Console.ReadLine());
                if (guess == c)
                { score++; }

                //division
                a = (int)(myrand.NextDouble() * 10) + 1;
                b = (int)(myrand.NextDouble() * 10) + 1;
                c = a * b;
                Console.Write("What is {0} / {1}? ", c, a);
                guess = Convert.ToInt32(Console.ReadLine());
                if (guess == b)
                {
                    score++;
                }

                Console.WriteLine("Score: {0} out of 4", score);
                switch (score)
                {
                    case 4:
                        Console.WriteLine("You're a genius!");
                        break;
                    case 3:
                        Console.WriteLine("You're pretty smart!");
                        break;
                    case 2:
                        Console.WriteLine("You could do better");
                        break;
                    case 1:
                        Console.WriteLine("You could use some practice");
                        break;
                    case 0:
                        Console.WriteLine("uhmm, try again");
                        break;

                }
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("Please press enter key to continue");
                Console.ReadLine();
            }
        }
    }
}